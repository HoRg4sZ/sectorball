# SECTORBALL #
##################


A simple 2D [Sector Ball](https://en.wikipedia.org/wiki/Button_football) game written using JavaFX (Java 8).


If you are using Eclipse, you need to download the [e(fx)clipse](http://www.eclipse.org/efxclipse/install.html#for-the-lazy) plugin in order to make it work.

To see the upcoming tasks and goals, read Tasks.txt
