package utils;

/**
 * Enum for selecting specific teams
 * @author mszekely
 *
 */
public enum TeamSelector {
    PREFILLEDTESTTEAM, ONEPLAYERTEAM, REDTEAM, BLUETEAM
}
