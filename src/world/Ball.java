package world;

import utils.Vector2D;

public class Ball extends Disk {

    /**
     * Reference to the player who touched the ball last time.
     */

    private Player whoTouchedLast;

    /**
     * Simple constructor
     * 
     * @param angle
     * @param angularSpeed
     * @param position
     * @param velocity
     */
    public Ball(double angle, double angularSpeed,Vector2D position, Vector2D velocity) {
        super(Parameters.BALLFRICTION, Parameters.BALLRADIUS, Parameters.BALLMASS, angle, angularSpeed,
                position, velocity);
        
        whoTouchedLast = new Player(0, 0, new Vector2D(0, 0), new Vector2D(0, 0), "Nobody");
    }

    public Ball() {
		setFriction(Parameters.BALLFRICTION);
		setRadius(Parameters.BALLRADIUS);
		setMass(Parameters.BALLMASS); 
    	whoTouchedLast = new Player();
	}

	public Player getWhoTouchedLast() {
        return whoTouchedLast;
    }

    public void setWhoTouchedLast(Player whoTouchedLast) {
        this.whoTouchedLast = whoTouchedLast;
    }

}
