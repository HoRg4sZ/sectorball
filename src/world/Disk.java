package world;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import utils.Vector2D;

/**
 * A class for all this like objects on the field, including the ball and the
 * players
 */
public class Disk extends RenderableObject{
    /**
     * The Disk's radius
     */
    private double radius;

    /**
     * The Disk's mass
     */
    private double mass;

    /**
     * the angle relative to...whatever I decide later
     * radians
     */
    private double angle = 0;

    /**
     * The angular speed
     */
    private double angularSpeed;

    /**
     * The friction
     */
    private double friction;

    /**
     * The disk's current velocity
     */
    private Vector2D velocity;

    /**
     * Flag for selection
     */

    private boolean selected = false;
    
    

    public Disk(double friction, double radius, double mass, double angle, double angularSpeed,
            Vector2D position, Vector2D velocity) {
        super(position,null);
        this.radius = radius;
        this.mass = mass;
        this.angle = angle;
        this.angularSpeed = angularSpeed;
        
        this.velocity = velocity;
        this.friction = friction;
    }
    
    public Disk()
    {
    	velocity = new Vector2D();
    }

    /**
     * Returns a new Disk instance. Copy constructor (it is needed in the Match
     * class).
     * 
     * @param disk
     * @return
     */
    
    public static Disk newInstance(Disk disk) {
        return new Disk(disk.friction, disk.radius, disk.mass, disk.angle, disk.angularSpeed,
                disk.getPosition(), disk.velocity);
    }

    /**
     * Simulating the change in position and velocity equations: deltaV =
     * friction * g * deltaT deltaX = (V - deltaV/2) * deltaT
     * 
     * @param deltaT
     */
    @Override
    public void simulate(double deltaT) {
        Vector2D deltaX;
        Vector2D deltaV;
        deltaV = velocity.getNorm().invert().scale(friction * Parameters.G * deltaT);
        if (deltaV.getLength() > velocity.getLength()) {
            velocity = new Vector2D(0, 0);
        } else {
            deltaX = velocity.subtract(deltaV.scale(0.5)).scale(deltaT);
            velocity = velocity.add(deltaV);
            setPosition(getPosition().add(deltaX));
        }
        // change in angle
        double deltaOmega;
        deltaOmega = friction * Parameters.G * deltaT / radius;
        angle += (angularSpeed - deltaOmega/2) * deltaT;
        angle = angle % (2 * Math.PI);
        angularSpeed += deltaOmega;
    }
    
    
    @Override
	public void render(GraphicsContext gc, Vector2D mousePos) {
    	 Vector2D pos = this.getPosition();
         Image playerIm = this.getSkin();
         gc.drawImage(playerIm, pos.x - this.getRadius(), pos.y - this.getRadius());
         /**
          * If the disk is selected, we draw selection ring around
          * it, and the line to show the velocity, the player can
          * give to the disk
          * We also indicate the speed that the player can give to the chosen disk, with a straight line from 
          * the cursor, to the disk
          */
         if (isSelected() == true && this instanceof Player) {
             gc.drawImage(new Image("/resources/select.png"), pos.x - getRadius() - 5,
                     pos.y - getRadius() - 5);
             gc.setLineWidth(3);
             gc.setStroke(Color.BLUE);
             gc.strokeLine(mousePos.x, mousePos.y, getPosition().x, getPosition().y);
         }
	}

    /**
     * String representation of the object for easier debugging.
     */
    @Override
    public String toString() {
        return super.toString() + " [radius=" + radius + ", mass=" + mass + ", angle=" + angle + ", angularSpeed="
                + angularSpeed + ", friction=" + friction + ", position=" + getPosition() + ", velocity=" + velocity + "]";
    }

    /*************************/
    /** Getters and Setters **/
    /*************************/
    
    public double getRadius() {
        return radius;
    }


    public void setRadius(double radius) {
        this.radius = radius;
    }


    public double getMass() {
        return mass;
    }


    public void setMass(double mass) {
        this.mass = mass;
    }


    public double getAngle() {
        return angle;
    }


    public void setAngle(double angle) {
        this.angle = angle;
    }


    public double getAngularSpeed() {
        return angularSpeed;
    }


    public void setAngularSpeed(double angularSpeed) {
        this.angularSpeed = angularSpeed;
    }


    public double getFriction() {
        return friction;
    }


    public void setFriction(double friction) {
        this.friction = friction;
    }


    public Vector2D getVelocity() {
        return velocity;
    }


    public void setVelocity(Vector2D velocity) {
        this.velocity = velocity;
    }


    public boolean isSelected() {
        return selected;
    }


    public void setSelected(boolean selected) {
        this.selected = selected;
    }
    
    public double getDiameter()
    {
        return this.radius * 2;
    }

}
