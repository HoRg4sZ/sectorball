package world;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.image.Image;
import utils.Pair;
import utils.TeamSelector;
import utils.Vector2D;

/**
 * A class to manage a match of two teams. It simulates the change of positions,
 * checks for goal, keeps the score Maybe too powerful?
 */
public class Match implements Runnable {

    /**
     * The playing teams
     */
    private Pair<Team> teams;
    
    /**
     * All objects on the field, including both teams, the ball, and the goals
     */

    private List<RenderableObject> renderableObjects;
    
    /**
     * All disks on the field 
     */
    private List<Disk> disks;
    
    /**
     * the ball
     */
    private Ball ball;
    
    /**
     * Manager class for collisions
     */
    private CollisionManager collisionManager;
    
    /**
     * The football pitch on which the other objects move
     * For now it is just an image, but it has a state and position
     */
    
    private Pitch pitch;
    
    /**
     * Goals on the pitch
     */
    
    private Goal leftGoal;
    
    private Goal rightGoal;

    /**
     * Current and previous system time
     */
    private double systemTime = 0;
    private double prevSystemTime = 0;
    /**
     * should the simulation run
     */
    private boolean run = true;

    /**
     * the percentage of impulse lost in a collision
     */
    private double l = 0.5;
    

    public Match(TeamSelector a, TeamSelector b) {
        
        pitch = new Pitch(Parameters.PITCHLENGTH, Parameters.PITCHHEIGHT);
        pitch.setPosition(new Vector2D(0,0));
        pitch.setSkin(new Image("/resources/fieldInFrame.png"));
        
        leftGoal = new Goal();
        leftGoal.setPosition(new Vector2D(world.Parameters.PITCHMARGIN, world.Parameters.PITCHHEIGHT/2));
        leftGoal.setSkin(new Image("/resources/goalLeft.png"));
        
        rightGoal = new Goal();
        rightGoal.setPosition(new Vector2D(world.Parameters.PITCHLENGTH - world.Parameters.PITCHMARGIN,
        		world.Parameters.PITCHHEIGHT/2));
        rightGoal.setSkin(new Image("/resources/goalRight.png"));
        
        teams = new Pair<Team>(new Team(a), new Team(b));
        
        renderableObjects = new ArrayList<RenderableObject>();
        setDisks(new ArrayList<Disk>());
        List<Player> playersOfA = teams.a.getPlayers();
        List<Player> playersOfB = teams.b.getPlayers();

        ball = new Ball();
        ball.setAngularSpeed(0);
        ball.setAngle(0);
        ball.setPosition(new Vector2D(500, 300));
        ball.setVelocity(new Vector2D());
        
        ball.setSkin(new Image("/resources/white_ball.png"));
        disks.addAll(playersOfA);
        disks.addAll(playersOfB);
        disks.add(ball);
        
        renderableObjects.add(pitch);
        renderableObjects.addAll(disks);
        renderableObjects.add(rightGoal);
        renderableObjects.add(leftGoal);
        
        collisionManager = new CollisionManager(renderableObjects, l);
         
    }

    /**
     * Adding a player to the list of renderable objects, disk
     * 
     * @param player
     */

    public void addPlayer(Player player) {
        renderableObjects.add(player);
        disks.add(player);
    }

    /**
     * Simulate the game for the given time. First we just simulate where the
     * renderable objects will be after the given time, then we check for collision, and
     * modify positions accordingly.
     * We try to simulate the world in every 1/1000 of a second - if for some reason
     * the simulation doesn't finish in 1/1000 second, we got a slightly different 
     * time step. 
     * 
     * @param delta
     */

    @Override
    public void run() {

        while (run) {
            systemTime = System.nanoTime() / 1000000000.0;
            double timeStep;

            if (prevSystemTime == 0) {
                timeStep = 0.0011;
            } else {
                timeStep = systemTime - prevSystemTime;
            }
            if (timeStep > 0.001) {
                List<Pair<RenderableObject>> collisions = new ArrayList<>();
                List<Disk> prevDisks = new ArrayList<>();
                List<RenderableObject> prevObjects = new ArrayList<>();
                Disk prevBall = ball;

                synchronized (renderableObjects) {
                    
                    
                    // Creating list to store the previous state of the Disks
					for (Disk disk : disks) {
                    	prevDisks.add(Disk.newInstance(disk));
                        if(disk.getClass().equals(Ball.class))
                        {
                        	prevBall = disk;
                        }
                    }
                    
					// Storing the previous state of all objects on the pitch
					prevObjects.add(pitch);
			        prevObjects.addAll(prevDisks);
			        prevObjects.add(rightGoal);
			        prevObjects.add(leftGoal);
					
                    // Simulating all the players and the ball
                    for (RenderableObject renderableObject : renderableObjects) {
                        renderableObject.simulate(timeStep);
                        // disk.getVelocity());
                    }
                    
                    
					collisionManager.createCollisions(collisions, prevObjects);
    
                    // we iterate through the list of collision to resolve them
                    for (Pair<RenderableObject> collision : collisions) {
                        collisionManager.collide(collision, timeStep);
                        collision.a.simulate(timeStep);
                        collision.b.simulate(timeStep);
                    }
    
                    // making sure that the players stay on the field
                    collideWithPitchBounderies(prevDisks);
                    // check if goal occurred
                    processGoal(prevBall);
                    prevSystemTime = systemTime;
                }
            }
        }
    }

    
	

    /**
     * Bounce players if they reach the end of the field
     * 
     * @param prevPlayers
     */
    private void collideWithPitchBounderies(List<Disk> prevPlayers) {
        for (int i = 0; i < getDisks().size(); i++) {
            Disk disk = getDisks().get(i);
            Vector2D pos = disk.getPosition();
            double radius = disk.getRadius();
            final Vector2D velocity = disk.getVelocity();
            if (pos.x < 0 || pos.x > pitch.getLenght() - radius) {
                Vector2D prevPos = prevPlayers.get(i).getPosition();
                if ((pos.x < radius && prevPos.x > pos.x) || (pos.x > pitch.getLenght() - radius && prevPos.x < pos.x)) {
                    velocity.x *= -0.4;
                    if (pos.x < radius) {
                        pos.x = radius;
                    }
                    if (pos.x > pitch.getLenght() - radius) {
                        pos.x = pitch.getLenght() - radius;
                    }
                    disk.setVelocity(velocity.scale(l));
                }
            }
            if (pos.y < radius || pos.y > pitch.getHeight() - radius) {
                Vector2D prevPos = prevPlayers.get(i).getPosition();
                if ((pos.y < radius && prevPos.y > pos.y) || (pos.y > pitch.getHeight() - radius && prevPos.y < pos.y)) {
                    velocity.y *= -0.4;
                    disk.setVelocity(velocity.scale(l));
                    if (pos.y < radius) {
                        pos.y = radius;
                    }
                    if (pos.y > pitch.getHeight() - radius) {
                        pos.y = pitch.getHeight() - radius;
                    }
                }
            }
        }
    }

    /**
     * Check if there was a goal, and set the score accordingly
     * TODO players must be reset, also animations and stuff
     * like that
     * @param prevBall 
     */
    private void processGoal(Disk prevBall) {
    	// TODO: here I presueme that the left goal belongs to team A, however this is not so good, it is not
    	// the Jedi way
    	
    	if(isOnThePitch(prevBall) == true)
    	{
    		if(isInsideGoal(leftGoal, ball) == true && isInsideGoal(leftGoal, prevBall) == false)
	    	{
	    		teams.a.scoreGoal();
	    	}
	    	if(isInsideGoal(rightGoal, ball) == true && isInsideGoal(rightGoal, prevBall) == false)
	    	{
	    		teams.b.scoreGoal();
	    	}
    	}
    }
    /**
     * Determine if the disk is inside the are of the given goal
     * @param goal
     * @param ball
     * @return
     */
    private boolean isInsideGoal(Goal goal, Disk ball)
    {
    	Vector2D goalPos = goal.getPosition();
    	Vector2D ballPos = ball.getPosition();
    	boolean xCoordinateValid = ballPos.x > goalPos.x && ballPos.x + goal.getLength() > ballPos.x;
		boolean yCoordinateValid = ballPos.y > goalPos.y && ballPos.y + goal.getHeight() > ballPos.y;
		if(xCoordinateValid && yCoordinateValid)
    	{
    		return true;
    	}
		else
		{
			return false;
		}
    }
    
    /**
     * 
     * @param ball
     * @return
     */
    private boolean isOnThePitch(Disk disk)
    {
    	Vector2D diskPos = disk.getPosition();
    	if((diskPos.x < Parameters.PITCHMARGIN || diskPos.x > Parameters.PITCHLENGTH - Parameters.PITCHMARGIN - disk.getRadius()) &&
    			(diskPos.y < Parameters.PITCHMARGIN || diskPos.y > Parameters.PITCHHEIGHT - Parameters.PITCHMARGIN - disk.getRadius()))
    	{
    		return false;
    	}
    	else
    	{
    		return true;
    	}
    }


    /*************************/
    /** Getters and setters **/
    /*************************/
    

    public List<RenderableObject> getRenderableObjects() {
        return renderableObjects;
    }

    public Disk getBall() {
        return ball;
    }

    public boolean isRun() {
        return run;
    }

    public void setRun(boolean run) {
        this.run = run;
    }

    public Pair<Team> getTeams() {
        return teams;
    }

    public void setTeams(Pair<Team> teams) {
        this.teams = teams;
    }

    public Pitch getPitch() {
        return pitch;
    }

    public void setPitch(Pitch pitch) {
        this.pitch = pitch;
    }

    public Goal getLeftGoal() {
        return leftGoal;
    }

    public void setLeftGoal(Goal leftGoal) {
        this.leftGoal = leftGoal;
    }

    public Goal getRightGoal() {
        return rightGoal;
    }

    public void setRightGoal(Goal rightGoal) {
        this.rightGoal = rightGoal;
    }

	public List<Disk> getDisks() {
		return disks;
	}

	public void setDisks(List<Disk> disks) {
		this.disks = disks;
	}

}
