package world;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import utils.Vector2D;

public abstract class RenderableObject {
    
    /**
     * The FieldObject's current position
     */
    private Vector2D position;
    
    /**
     * The FieldObejct's skin
     */

    private Image skin;

    public RenderableObject() {
    	
    }
    
    public RenderableObject(Vector2D position, Image skin) {
        this.position = position;
        this.skin = skin;
    }
    
    
    /**
     * Simulating the change in the objects state
     */
    protected abstract void simulate(double deltaT);
    
    /**
     * Rendering the object
     */
    public abstract void render(GraphicsContext gc, Vector2D mousePos);
    
    /**
     * Get the disk's current position
     * 
     * @return
     */

    public Vector2D getPosition() {
        return position;
    }
    
    /**
     * Set the disk's current position
     * 
     * @param position
     */
    public void setPosition(Vector2D position) {
        this.position = position;
    }

    public Image getSkin() {
        return skin;
    }

    public void setSkin(Image skin) {
        this.skin = skin;
    }
    
   
    
}
