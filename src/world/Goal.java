package world;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import utils.Vector2D;

public class Goal extends RenderableObject {
  
    private final double length;
    private final double height;
    
    
    public Goal(Vector2D position, Image skin) {
        super(position, skin);
        length = Parameters.GOALLENGTH;
        height = Parameters.GOALHEIGHT;
    }

    public Goal() {
    	length = Parameters.GOALLENGTH;
        height = Parameters.GOALHEIGHT;
	}

    /**
     * Determines if a Disk is currently colliding with the Goal
     * (if it is too close to it)
     * @param disk
     * @return
     */
    public boolean isDiskCollided(Disk disk) {
    	final Vector2D pos = getPosition();
    	boolean collided;
    	collided = isInRange(disk.getPosition().x, pos.x - disk.getRadius(), pos.x + disk.getRadius()) &&
    	    	   isInRange(disk.getPosition().y, pos.y - disk.getRadius(), pos.y + disk.getRadius());
    	return collided;
    }
    
    private boolean isInRange(double x, double lowerLimit, double upperLimit) {
    	return x > lowerLimit && x < upperLimit;
    }
    

	@Override
	public void render(GraphicsContext gc, Vector2D mousePos) {
		Vector2D position = getPosition();
		gc.drawImage(getSkin(),
        		position.x  - Parameters.GOALLENGTH/2,
        		position.y - Parameters.GOALHEIGHT/2);
	}
    
    
    /**
     * This should simulate the movement of the net
     */
    
    @Override
    protected void simulate(double deltaT) {
        // TODO Auto-generated method stub
    }
    
    public double getLength() {
        return length;
    }

    public double getHeight() {
        return height;
    }
    
}
