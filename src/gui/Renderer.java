package gui;

import java.util.List;

import javafx.animation.AnimationTimer;
import javafx.scene.canvas.GraphicsContext;
import utils.Vector2D;
import world.Match;
import world.RenderableObject;

/**
 * This class implements the game loop, it runs at every 1/60 of a second,
 * or as close to that as possible. It gets the new position of the
 * objects on the field and renders all them, and everything else that
 * needs to be rendered.
 */

public class Renderer extends AnimationTimer {

    private GraphicsContext gc;
    private Match match;
    private Vector2D mousePos;
    
    public Renderer(GraphicsContext gc, Match match, Vector2D mousePos)
    {
        this.gc = gc;
        this.match = match;
        this.mousePos = mousePos;
    }
    
    @Override
    public void handle(long currentNanoTime) {
        
        List<RenderableObject> renderableObjects = match.getRenderableObjects();
        
        synchronized(renderableObjects)
        {
            for (RenderableObject renderableObject : renderableObjects) {
            	renderableObject.render(gc, mousePos);
            }
        }
        
        
    }

}
