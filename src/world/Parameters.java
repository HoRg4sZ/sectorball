package world;

/**
 * Class to store all the parameters and constants, that describe the world 
 * @author Miki
 *
 */
public class Parameters {
	/**
	 * Gravitational acceleration
	 */
	public static final double G = 9.80665;
	/**
	 * Mass of the players
	 */
	public static final double PLAYERMASS = 1000d;
	/**
	 * Friction between the players and the table
	 * @return
	 */
	public static final double PLAYERFRICTION = 0.8d;
	
	/**
	 * Mass of the ball
	 */
	public static final double BALLMASS = 50d;
	/**
	 * Friction between the ball and the table
	 * @return
	 */
	public static final double BALLFRICTION = 0.9d;
	
	/**
	 * Radius of the players
	 * @return
	 */
	public static final double PLAYERRADIUS = 20;
	
	/**
	 * Radius of the ball
	 * @return
	 */
	public static final double BALLRADIUS = 10;
	
	/**
	 * Height of the pitch
	 * @return
	 */
	public static final double PITCHHEIGHT = 760;
	
	/**
     * Length of the pitch
     * @return
     */
    public static final double PITCHLENGTH = 1130;
    
    /**
     * Length of the goal
     */
    
    public static final double GOALLENGTH = 30;
    
    /**
     * Height of the goal
     */
    
    public static final double GOALHEIGHT = 100;
    
    /**
     * The width of the empty area around the pitch
     */
    public static final double PITCHMARGIN = 25;



}
