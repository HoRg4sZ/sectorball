package world;

import java.util.ArrayList;
import java.util.List;

import utils.Pair;
import utils.Vector2D;

/**
 * This class does everything that is related to collisions on the pitch
 * @author mszekely
 *
 */
public class CollisionManager {
	
	private List<RenderableObject> renderableObjects;
	private double l;
	private List<RenderableObject> prevObjects;
	
	public CollisionManager(List<RenderableObject> renderableObjects, double l) {
		this.renderableObjects = renderableObjects;
		this.l = l;
		prevObjects = new ArrayList<>();
	}

	/**
     * We create a list of all the collisions. First we look for objects
     * closer to each other than they should be, than we check if the collision
     * has already happened by checking if they are getting further to each other.
     * If they do not, than we have to collide them, so we add them to the list.
     * It's not perfect because this way the disks actually slip into each other.
     * To fix this, we use the rollBackToCollision method, to roll back the state of
     * the world to the actual point of the collision.
     *  
     * @param collisions
     * @param prevlayers
     */

    public void createCollisions(List<Pair<RenderableObject>> collisions, List<RenderableObject> previousStateOfObjects) {
        Vector2D distanceVector;
        boolean isInRadius;
        Pair<RenderableObject> newCollision;
        prevObjects = previousStateOfObjects;

        for (RenderableObject renderableObject : renderableObjects) {
            for (RenderableObject otherObject : renderableObjects) {
                if (!renderableObject.equals(otherObject)) {
                	if(renderableObject instanceof Player || renderableObject instanceof Ball)
                	{
                		Disk disk = (Disk) renderableObject;
                		if(otherObject instanceof Player || otherObject instanceof Ball)
                		{
                			Disk otherDisk = (Disk) otherObject;
                			distanceVector = renderableObject.getPosition().subtract(otherObject.getPosition());
    	                    isInRadius = distanceVector.getLength() < (disk.getRadius() + otherDisk.getRadius());
    	                    if (isInRadius && isGettingCloser(disk, otherDisk)) {
    	                        newCollision = new Pair<RenderableObject>(renderableObject, otherObject);
    	                        boolean contains = false;
    	
    	                        for (Pair<RenderableObject> collision : collisions) {
    	                            if (collision.a.equals(newCollision.b)
    	                                    && collision.b.equals(newCollision.a)) {
    	                                contains = true;
    	                            }
    	                        }
    	                        if (contains == false) {
    	                            collisions.add(newCollision);
    	                        }
    	                    }
                		}
                		else
                		{
                			if(otherObject instanceof Goal)
                			{
                				Goal goal = (Goal) otherObject;
                				if(goal.isDiskCollided(disk) && isGettingCloser(disk, goal))
                				{
                					newCollision = new Pair<RenderableObject>(renderableObject, otherObject);
        	                        collisions.add(newCollision);
                				}
                			}
                		}
                	}
	            }
            }
        }
    }
    
    /**
     * Determines if the two object are getting closer to each other
     * @param objectA
     * @param objectB
     * @return
     */
    private boolean isGettingCloser(RenderableObject objectA, RenderableObject objectB) {
    	Vector2D distance = objectA.getPosition().subtract(objectB.getPosition());
    	Vector2D prevDist = prevObjects.get(renderableObjects.indexOf(objectA)).getPosition();
        prevDist = prevDist.subtract(prevObjects.get(renderableObjects.indexOf(objectB)).getPosition());
        return distance.getLength() < prevDist.getLength();
    }
    
    
    /**
     * Colliding 2 disks. First we roll back the state of the players to the
     * point where they collided. Then we calculate the velocity of the objects
     * after the collision using the conservation of momentum and the
     * conservation of energy equations. Then we scale the vectors by l, which
     * is the percentage of energy loss in the collision. (Converted into heat,
     * etc...)
     * 
     * @param collision
     * @param delta
     */
    public void collide(Pair<RenderableObject> collision, double delta) {

        // roll back the state of the two disks till the collision
        collision = rollBackToCollision(collision, delta);
    	
        RenderableObject a = collision.a;
		if(a instanceof Player || a instanceof Ball)
    	{
    		Disk diskA = (Disk) a;
    		RenderableObject b = collision.b;
			if(b instanceof Player || b instanceof Ball)
    		{
    			Disk diskB = (Disk) b;
	
		        final Vector2D diskAPosition = diskA.getPosition();
		        final Vector2D diskBPosition = diskB.getPosition();
		        
		        // calculating the normal vector of the radial component of the velocity
		        final Vector2D normA = diskAPosition.subtract(diskBPosition).getNorm();
		        final Vector2D normB = diskBPosition.subtract(diskAPosition).getNorm();
		
		        Vector2D diskAVelocity = diskA.getVelocity();
		        Vector2D diskBVelocity = diskB.getVelocity();
		       
		        // angle between radial component of velocity and velocity
		        final double angleA = Vector2D.getAngle(normA, diskBVelocity);
		        final double angleB = Vector2D.getAngle(normB, diskAVelocity);
		        
		
		        // radial components
		        final Vector2D radA = normB.scale(diskAVelocity.getLength() * Math.cos(angleA));
		        final Vector2D radB = normA.scale(diskBVelocity.getLength() * Math.cos(angleB));
		        
		
		        // tangential components
		        final Vector2D tanA = diskAVelocity.subtract(radA);
		        final Vector2D tanB = diskBVelocity.subtract(radB);
		        
		        // modifying angular speed
		        diskA.setAngularSpeed(tanA.getLength() * l);
		        diskB.setAngularSpeed(tanB.getLength() * l);
		        
		        // calculating the radial components after the collision
		        diskAVelocity = radA;
		        diskBVelocity = radB;
		
		        final double diskAMass = diskA.getMass();
		        final double diskBMass = diskB.getMass();
		        Vector2D radialA = diskAVelocity.scale(diskAMass - diskBMass);
		        radialA = radialA.add(diskBVelocity.scale(2 * diskBMass));
		        radialA = radialA.scale(1 / (diskAMass + diskBMass));
		        Vector2D radialB = diskBVelocity.scale(diskBMass - diskAMass);
		        radialB = radialB.add(diskAVelocity.scale(2 * diskAMass));
		        radialB = radialB.scale(1 / (diskAMass + diskBMass));
		
		        // summing the radial and the tangential components
		        diskA.setVelocity(radialA.add(tanA).scale(l));
		        diskB.setVelocity(radialB.add(tanB).scale(l));
    		}
			else if(b instanceof Goal) {
				
				diskA.setVelocity(diskA.getVelocity().invert().scale(l));
			}
    	}
        
    }
    

    /**
     * We want to find the moment they crashed. We do something like this:
     * current positions: pos1,pos2 v1, v2 - invert speed vectors what has to be
     * true: (pos1 + v1 * t - (pos2 + v2 * t)).length == radius * 2 ==> (pos1 -
     * pos2 + v1 * t - v2 * t).length == radius * 2 ==> (pos1 - pos2 + (v1 - v2)
     * * t).length == radius * 2 ==> it will never be equal - checking if the
     * difference is smaller than epsilon we want to find t, in order to get the
     * right velocity vectors with which we can modify the position vectors
     * 
     * @param collision
     * @param delta
     * @return
     */

    public Pair<RenderableObject> rollBackToCollision(Pair<RenderableObject> collision, double delta) {
        
    	RenderableObject a = collision.a;
    	final double step = delta / 100d;
    	if(a instanceof Player || a instanceof Ball)
    	{
    		Disk diskA = (Disk) a;
    		RenderableObject b = collision.b;
			if(b instanceof Player || b instanceof Ball)
    		{
    			Disk diskB = (Disk) b;
    	
		    	
		        final Vector2D diskAPosition = diskA.getPosition();
		        final Vector2D diskBPosition = diskB.getPosition();
		        final Vector2D diskAVelocity = diskA.getVelocity();
		        final Vector2D diskBVelocity = diskB.getVelocity();
		
		        final Vector2D posDiff = diskAPosition.subtract(diskBPosition);
		        final Vector2D velSum = diskAVelocity.invert().subtract(diskBVelocity.invert());
		        
		        final double epsilon = 0.01;
		        // the distance between the 2 disks
		        final double radSum = diskA.getRadius() + diskB.getRadius();
		        double distance = 0;
		        boolean collisionFound = false;
		        double i;
		        for (i = 0; !collisionFound && Math.abs(distance) < radSum; i += step) {
		            distance = posDiff.add(velSum.scale(i)).getLength();
		            if (Math.abs(radSum - distance) < epsilon) {
		                collisionFound = true;
		            }
		        }
		
		        
		
		        // rolling back position
		        Vector2D deltaXA = diskAVelocity.invert().scale(i);
		        diskA.setPosition(diskAPosition.add(deltaXA));
		        Vector2D deltaXB = diskBVelocity.invert().scale(i);
		        diskB.setPosition(diskBPosition.add(deltaXB));
		
		        // rolling back velocity
		        final Vector2D decA = diskAVelocity.getNorm().invert().scale(9.81d * diskA.getFriction() * i);
		        final Vector2D decB = diskBVelocity.getNorm().invert().scale(9.81d * diskB.getFriction() * i);
		        diskA.setVelocity(diskAVelocity.add(decA));
		        diskB.setVelocity(diskBVelocity.add(decB));
		        
    		} else if(b instanceof Goal) {
    			Goal goal = (Goal) b;
    			Vector2D velInv = diskA.getVelocity().invert();
    			boolean notInsideGoal = true;
				Vector2D posA;
    			for (int i = 0; notInsideGoal; i += step) {
					posA = diskA.getPosition().add(velInv.scale(i));
					diskA.setPosition(posA);
					if(goal.isDiskCollided(diskA))
					{
						notInsideGoal = false;
					}
				}
    		}
    	
    		
    	}
		// TODO nem tuti hogy ide k�ne
		return collision;
    }
}
