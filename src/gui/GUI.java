package gui;

import java.util.List;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.stage.Stage;
import utils.TeamSelector;
import utils.Vector2D;
import world.Disk;
import world.Match;
import world.Player;
import world.Team;

/**
 * A simple class to test the physics in JavaFX
 */

public class GUI extends Application {

    /**
     * If there is a disk selected by the user
     */

    private boolean isThereSelected = false;

    /**
     * The disk selected by the user
     */
    private Disk selected = null;

    /**
     * Position of the mouse cursor, updated in event handler
     */
    private static Vector2D mousePos;

    /**
     * The match being played
     */

    private static Match match;

    /**
     * Main method, it
     *  creates the match
     *  starts the simulation of the world in another thread
     *  launches the UI
     * @param args
     */
    public static void main(String[] args) {
        match = new Match(TeamSelector.REDTEAM, TeamSelector.BLUETEAM);
        mousePos = new Vector2D();
        Thread simulation = new Thread(match, "simulation");
        simulation.start();
        launch(args);
    }

    /**
     * This method is called by the launch method, which waits for this to end,
     * then runs the stop method Basically every UI related initialization and
     * operation is executed here
     */
    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Physics Test");
        Group root = new Group();
        Scene theScene = new Scene(root);
        stage.setScene(theScene);

        
        Canvas canvas = new Canvas(world.Parameters.PITCHLENGTH, world.Parameters.PITCHHEIGHT);
        root.getChildren().add(canvas);

        GraphicsContext gc = canvas.getGraphicsContext2D();

        /**
         * This runs when somebody closes the application. Mainly needed to kill
         * the thread that simulates the world.
         */

        stage.setOnCloseRequest(e -> {
            Platform.exit();
            match.setRun(false);
        });

        /**
         * Creating an event handler for clicking with the mouse on the field
         * using a mighty lambda expression
         * First, the two players can place their team on the field, after that
         * the game begins
         * TODO: might be better in its own class
         */

        
        
        theScene.setOnMouseClicked(

                event -> {
                    Team teamA = match.getTeams().a;
                    int numberOfPlayers = teamA.getPlayers().size();
                    // refresh the currently selected player
                    if (selected == null)
                    {
                        selected = findSelected(match.getDisks(),
                                new Vector2D((double) event.getX(), (double) event.getY()));
                    }
                    // check if we clicked try to place the player to an already taken position
                    Disk clickedOnOtherDisk = findSelected(match.getDisks(),
                                new Vector2D((double) event.getX(), (double) event.getY()));
                    if (numberOfPlayers < teamA.getMaxNumberOfPlayers())
                    {
                        if (clickedOnOtherDisk == null)
                        {
                            Player newPlayer = new Player();
                            newPlayer.setAngle(0);
                            newPlayer.setAngularSpeed(0);
                            newPlayer.setPosition(new Vector2D(event.getX(), event.getY()));
                            newPlayer.setName("Player" + numberOfPlayers);
                            // 0, 0, new Vector2D(event.getX(), event.getY()),new Vector2D(), "Player" + numberOfPlayers
                            teamA.addPlayer(newPlayer);
                            match.addPlayer(newPlayer);
                        }
                    }
                    else
                    {
                        Team teamB = match.getTeams().b;
                        numberOfPlayers = teamB.getPlayers().size();
                        if (numberOfPlayers < teamB.getMaxNumberOfPlayers())
                        {
                            if (clickedOnOtherDisk == null)
                            {
                                Player newPlayer = new Player();
                                newPlayer.setAngle(0);
                                newPlayer.setAngularSpeed(0);
                                newPlayer.setPosition(new Vector2D(event.getX(), event.getY()));
                                newPlayer.setName("Player" + numberOfPlayers);
                                //0, 0, new Vector2D(event.getX(), event.getY()),new Vector2D(), "Player" + numberOfPlayers
                                teamB.addPlayer(newPlayer);
                                match.addPlayer(newPlayer);
                            }
                        }
                        else
                        {
                            if (isThereSelected == false)
                            {
                                if (selected != null)
                                {
                                    isThereSelected = true;
                                    selected.setSelected(true);
                                    mousePos.x = event.getX();
                                    mousePos.y = event.getY();
                                }
                            }
                            else
                            {
                                Vector2D v = new Vector2D((double) event.getX(), (double) event.getY());
                                selected.setVelocity(selected.getPosition().subtract(v));
                                isThereSelected = false;
                                selected.setSelected(false);
                                selected = null;
                            }
                        }
                    }
                });

        /**
         * Mouse movement event handler, we update the stored position of the
         * mouse is there is a selected disk
         */

        theScene.setOnMouseMoved(event -> {
            if (isThereSelected) {
                mousePos.x = event.getX();
                mousePos.y = event.getY();
            }
        });

        /**
         * Creating and starting the renderer
         */
        Renderer renderer = new Renderer(gc, match, mousePos);
        renderer.start();
        stage.show();
    }

    /**
     * Returns the (first) disk selected by the user If there is no Disk
     * selected, it returns null
     * 
     * @param players
     * @param clickPos
     * @return
     */

    private Disk findSelected(List<Disk> players, Vector2D clickPos) {
        for (Disk disk : players) {
            if (disk.getPosition().subtract(clickPos).getLength() < disk.getDiameter()) {
                return disk;
            }
        }
        return null;
    }

}
