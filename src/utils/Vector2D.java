package utils;

/**
 * A class for two dimensional mathematical vectors. I'm sure there are
 * wonderful implementations for it, but I wanted to do my own without a bunch
 * of unused methods.
 */
public class Vector2D {

    public double x;
    public double y;

    public Vector2D() {
        x = 0;
        y = 0;
    }

    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Vector2D add(Vector2D v) {
    	return new Vector2D(x + v.x, y + v.y);
    }

    public Vector2D subtract(Vector2D v) {
        return new Vector2D(x - v.x, y - v.y);
    }

    public Vector2D scale(double s) {
        return new Vector2D(x * s, y * s);
    }

    public double getLength() {
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
    }

    /**
     * Creates the normalized version of the vector
     * 
     * @return
     */
    public Vector2D getNorm() {
        final double length = getLength();
        Vector2D norm = this;
        if (length != 0) {
            norm = this.scale(1 / length);
        }
        return norm;
    }

    public Vector2D invert() {
        return new Vector2D(x * -1, y * -1);
    }

    /**
     * Returns the scalar product
     * 
     * @param a
     * @param b
     * @return
     */
    public static double scalarProduct(Vector2D a, Vector2D b) {
        return a.x * b.x + a.y * b.y;
    }

    /**
     * Returns the angle between the two vectors
     * 
     * @param a
     * @param b
     * @return
     */
    public static double getAngle(Vector2D a, Vector2D b) {
        double scalar = scalarProduct(a, b);
        double angle;
        if (scalar == 0) {
            angle = 0;
        } else {
            angle = Math.acos(scalar = scalar / (a.getLength() * b.getLength()));
        }
        return angle;
    }

    @Override
    public String toString() {
    	return "(" + x + ", " + y + ")";
    }
}
