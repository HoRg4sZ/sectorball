package utils;

/**
 * This enumeration is used to decide which skin should be loaded for a player
 * currently unused
 * 
 * @author mszekely
 *
 */

public enum SkinSelector {
    BASIC, METAL, BODE
}
