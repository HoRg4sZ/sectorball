package world;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.image.Image;
import utils.TeamSelector;
import utils.Vector2D;

public class Team {

    /**
     * Players of the team
     */

    private List<Player> players;

    private static final int maxNumberOfPlayers = 11;

    private Image teamSkin;

    /**
     * Number of goals scored
     */

    private int score = 0;

    private TeamSelector teamType;

    public Team(TeamSelector team) {
        players = new ArrayList<Player>();
        setScore(0);
        Player newPlayer;
        switch (team) {

        /**
         * Fill the team with players for testing
         */
        case PREFILLEDTESTTEAM:
            newPlayer = new Player(0, 0, new Vector2D(700, 360), new Vector2D(), "Player0");
            newPlayer.setTeam(this);
            setSkin(new Image("/resources/button_red.png"));
            addPlayer(newPlayer);

            final int rows = 3;
            final int columns = 3;
            // matrix of Players
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < columns; j++) {
                    Vector2D playerStartPosition = new Vector2D(330 + 50 * i, 200 + 50 * j);
                    newPlayer = new Player();
                    newPlayer.setAngle(0);
                    newPlayer.setAngularSpeed(0);
                    newPlayer.setPosition(playerStartPosition);
                    newPlayer.setVelocity(new Vector2D());
                    newPlayer.setName("Player" + i);
                    // 0, 0, playerStartPosition, new Vector2D(), "Player" + i
                    newPlayer.setTeam(this);
                    addPlayer(newPlayer);
                }
            }
            Vector2D playerStartPosition = new Vector2D(200, 240);
            newPlayer = new Player(0, 0, playerStartPosition, new Vector2D(), "Player10");
            newPlayer.setVelocity(new Vector2D(200, -20));
            addPlayer(newPlayer);
            break;
        case ONEPLAYERTEAM:
            newPlayer = new Player(0, 0, new Vector2D(50, 50), new Vector2D(), "Player0");
            setSkin(new Image("/resources/button_red.png"));
            players.add(newPlayer);
            break;
        case REDTEAM:
            setSkin(new Image("/resources/button_red.png"));
            break;
        case BLUETEAM:
            setSkin(new Image("/resources/button_blue.png"));
            break;

        }
        teamType = team;
    }

    /**
     * Adding a player to the team Setting skin to the teams skin returns a
     * boolean to show if the adding operation was successful
     * 
     * @param player
     */

    public boolean addPlayer(Player player) {
        if (players.size() <= maxNumberOfPlayers) {
            player.setSkin(teamSkin);
            players.add(player);
            return true;
        } else {
            return false;
        }
    }
    
    public void scoreGoal()
    {
    	score++;
    }
    
    public List<Player> getPlayers() {
        return players;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Image getSkin() {
        return teamSkin;
    }

    public void setSkin(Image skin) {
        this.teamSkin = skin;
    }

    public TeamSelector getTeamType() {
        return teamType;
    }

    public int getMaxNumberOfPlayers() {
        return maxNumberOfPlayers;
    }

}
