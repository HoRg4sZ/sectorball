package world;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import utils.Vector2D;

public class Pitch extends RenderableObject {

    private final double height;
    private final double lenght;
    
    public Pitch(Vector2D position, Image skin, double lenght, double height) {
        super(position, skin);
        this.height = height;
        this.lenght = lenght;
    }

    public Pitch(double lenght, double height) {
    	this.height = height;
    	this.lenght = lenght;
	}

	/**
     * This should simulate the change of the pitch (powder effect, etc)
     */
    
    @Override
    protected void simulate(double deltaT) {
        // TODO Auto-generated method stub
        
    }
    

	@Override
	public void render(GraphicsContext gc, Vector2D mousePos) {
		Vector2D position = getPosition();
		Image skin = getSkin();
		gc.drawImage(skin, position.x, position.y);
	}

    public double getHeight() {
        return height;
    }

    public double getLenght() {
        return lenght;
    }
    
}
