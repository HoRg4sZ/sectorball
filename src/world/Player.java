package world;

import utils.Vector2D;

public class Player extends Disk {

    /**
     * Name of the player
     */
    private String name;

    /**
     * Number of goals scored by the player
     */

    private int goalsScored = 0;

    /**
     * The players team
     */

    private Team team;

    /**
     * Constructor, that fills all parameters
     * 
     * @param angle
     * @param angularSpeed
     * @param angularAcceleration
     * @param position
     * @param velocity
     * @param acceleration
     * @param name
     */

    public Player(double angle, double angularSpeed, Vector2D position, Vector2D velocity, String name) {
        super(Parameters.PLAYERFRICTION, Parameters.PLAYERRADIUS, Parameters.PLAYERMASS, angle, angularSpeed,
                position, velocity);
        this.setName(name);
    }

    public Player() {
		setFriction(Parameters.PLAYERFRICTION);
		setRadius(Parameters.PLAYERRADIUS);
		setMass(Parameters.PLAYERMASS);
	}

	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public int getGoalsScored() {
        return goalsScored;
    }

    /**
     * Increase the number of goals scored by one
     */

    public void scored() {
        this.goalsScored++;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }


}
