package utils;

/**
 *  Very simple class to store a pair of disks which collide
 */

public class Pair<P> {
    public P a;
    public P b;

    public Pair(P a, P b) {
        this.a = a;
        this.b = b;
    }

}
